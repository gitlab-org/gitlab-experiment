### 0.7.0 Changelog

#### Added

- Add an improved "run" callback interface with `before_run`, `after_run` and `around_run`
- Add a configuration for strict experiment registration (disallow anonymous experiments without being backed by a class definition)
- Add class level interface for defining the experiment behaviors
- Add initial capability for rollout strategies to assign control
- Add `register_behavior` rspec matcher

#### Fixed

- Fixed compatibility for ruby 2.6.x
- Fixed issues with stubbing and the `track` rspec matcher

### Changed

- Allow `publish` to be called without a result argument

#### Deprecated

- The use of `use { }` and `try { }` has been deprecated, instead use `control { }`, `candidate { }` and `variant(:named_variant) { }`
- Passing a named variant argument to `candidate { }` (or `try { }`) has been deprecated, instead use `variant(:named_variant) { }`
- Calling `variant` (as in `variant.name` to get, or `variant(:assigned_variant)` to set) has been deprecated, instead use `assigned` (as in `assigned.name` to get the variant that has been assigned, and `assigned(:assigned_variant)` to manually set one)
- Using public `*_behavior` methods has been deprecated, instead declare the experiment in the class using `control`, `candidate` and `variant(:variant_name)` registration methods
- Configuration of `context_hash_strategy` has been deprecated, instead use `context_key_secret` and `context_key_bit_length` configuration options
- Configuration `variant_resolver`, and `inclusion_resolver` options have been deprecated, instead put this logic into custom rollout strategies
- The `experiment_group?` method has been deprecated, which may have been overridden in subclasses, instead put this logic into custom rollout strategies
- The `flipper_id` and `session_id` methods have been deprecated, which may have been overridden in subclasses, instead put this logic into custom rollout strategies

### Removed

- Removed scientist as a gem dependency

### Other

- Project has been moved to https://gitlab.com/gitlab-org/ruby/gems/gitlab-experiment
