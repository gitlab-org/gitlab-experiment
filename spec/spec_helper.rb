# frozen_string_literal: true

require 'bundler/setup'
require 'simplecov-cobertura'
require 'pry'
require 'redis'
require 'rspec-parameterized-table_syntax'

if ENV.fetch('INCLUDE_RAILS', 'true') == 'true'
  # Configure and start SimpleCov.
  SimpleCov.minimum_coverage 100
  SimpleCov.start 'rails' do
    formatter SimpleCov::Formatter::CoberturaFormatter if ENV['CI']
    add_filter 'vendor/cache' # ignore the cache on ci
    add_filter 'experiment/version' # can't be covered, loaded by the gemspec
    add_filter 'release/' # ignore release code until it's working properly

    add_filter ENV['SIMPLE_COV_FILTERS'] if ENV['SIMPLE_COV_FILTERS']
  end

  # Load Rails and spec dependencies.
  ENV['RAILS_ENV'] ||= 'test'
  require_relative 'dummy/environment'
  require 'rspec/rails'
  require 'generator_spec'

  # Require all generators so we can test them.
  Dir[File.expand_path('../lib/generators/**/*.rb', __dir__)].each { |f| require f }
end

require 'gitlab/experiment'
require 'gitlab/experiment/rspec'
require 'active_support/time_with_zone' # Time.current
require 'active_support/core_ext/numeric/time' # 3.weeks.ago
require 'active_support/core_ext/hash/keys' # { a: :b }.deep_stringify_keys

# Require all support files.
Dir[File.expand_path('support/**/*.rb', __dir__)].each { |f| require f }

# Get a connection to Redis.
REDIS_CURRENT = Redis.new(host: ENV.fetch('REDIS_HOST', nil), url: ENV.fetch('REDIS_URL', nil))

RSpec.configure do |config|
  config.order = :random
  config.raise_on_warning = true

  config.disable_monkey_patching!
  config.raise_errors_for_deprecations!

  config.expect_with :rspec do |expectations|
    expectations.syntax = :expect # Disable `should`.
  end

  config.mock_with :rspec do |mocks|
    mocks.syntax = :expect # Disable `should_receive` and `stub`.
  end

  # Enable flags like --only-failures and --next-failure.
  config.example_status_persistence_file_path = '.rspec_status'

  config.before do
    RequestStore.clear!
    Gitlab::Experiment.configure do |c|
      c.name_prefix = 'gitlab_experiment'
      c.logger = Logger.new($stdout).tap { |l| l.level = :warn }
    end
  end
end
