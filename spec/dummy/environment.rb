# frozen_string_literal: true

ENV['BUNDLE_GEMFILE'] ||= File.expand_path('../../Gemfile', __dir__)
require 'bundler/setup'

require 'action_controller/railtie'
require 'action_mailer/railtie'

Bundler.require(:test)

require 'gitlab/experiment'

# Configure gitlab-experiment.
Gitlab::Experiment.configure do |config|
  config.mount_at = '/glex'
end

# Define the Rails application.
module Dummy
  class Application < Rails::Application
    config.eager_load = false
  end
end

# Register our routes.
Rails.application.routes.draw do
  default_url_options host: 'localhost', port: 3000, script_name: ''
end

# Initialize the Rails application.
Rails.application.initialize!

# Global scope! For a nicer console (I KNOW!).
if Rails.env.development?
  include Rails.application.routes.url_helpers
  include Gitlab::Experiment::Dsl # rubocop:disable Style/MixinUsage
end
