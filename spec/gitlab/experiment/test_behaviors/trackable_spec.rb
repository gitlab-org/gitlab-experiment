# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Gitlab::Experiment::TestBehaviors::Trackable, :experiment do
  let(:tracked) { Gitlab::Experiment::TestBehaviors::TrackedStructure }

  before do
    allow(config).to receive_messages(base_class: 'NestableExperiment', name_prefix: nil)
  end

  describe "a nested and tracked scenario" do
    let(:multi_level_nested_experiment) do
      experiment(:ex1) { |e| e.control { first_level_nested && first_level_nested_no_children } }
    end

    let(:first_level_nested) do
      experiment(:ex1_1) { |e| e.control { second_level_nested && experiment(:ex1_1_1) {} } }
    end

    let(:second_level_nested) { experiment(:ex1_1_1) {} }
    let(:first_level_nested_no_children) { experiment(:ex1_2) {} }
    let!(:one_level_nested_experiment) { experiment(:ex2) {} }

    it "maintains a nesting structure that can be evaluated holistically" do
      multi_level_nested_experiment.run
      expect(tracked.hierarchy.deep_stringify_keys).to eq(JSON.parse(<<~JSON))
        {
          "ex2": {"name": "ex2", "count": 1, "children": {}},
          "ex1": {
            "name": "ex1",
            "count": 1,
            "children": {
              "ex1_1": {
                "name": "ex1_1",
                "count": 1,
                "children": {
                  "ex1_1_1": {"name": "ex1_1_1", "count": 2, "children": {}},
                  "ex1_2": {"name": "ex1_2", "count": 1, "children": {}
                  }
                }
              }
            }
          }
        }
      JSON
    end

    it "tracks a list of dependencies for all experiments that have been run" do
      multi_level_nested_experiment.run
      expect(tracked.dependencies.deep_stringify_keys).to eq(JSON.parse(<<~JSON))
        {
          "ex2": [],
          "ex1": [],
          "ex1_1": ["ex1"],
          "ex1_1_1": ["ex1", "ex1_1"],
          "ex1_2": ["ex1"]
        }
      JSON
    end
  end
end
