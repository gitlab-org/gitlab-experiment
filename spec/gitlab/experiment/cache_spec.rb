# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Gitlab::Experiment::Cache do
  let(:subject_experiment) { Gitlab::Experiment.new(:example, version: 3) }
  let(:store) { double(fetch: nil, read: nil, write: nil, delete: nil) }
  let(:keys) do
    {
      version1: "#{subject_experiment.name}:#{key_for(subject_experiment, version: 1)}",
      version2: "#{subject_experiment.name}:#{key_for(subject_experiment, version: 2)}",
      version3: "#{subject_experiment.name}:#{key_for(subject_experiment, version: 3)}"
    }
  end

  before do
    allow(config).to receive(:cache).and_return(store)
  end

  it "provides a way to specify the cache store to use" do
    expect(subject_experiment.cache.store).to eq(config.cache)
  end

  it "generates a cache key" do
    expect(subject_experiment.cache_key).to eq(keys[:version3])
  end

  it "fetches the variant from the cache" do
    expect(store).to receive(:fetch).with(keys[:version3]).and_return('_version_3_')

    expect(subject_experiment.cache_variant { '_new_value_' }).to eq('_version_3_')
  end

  it "updates the cached value when it doesn't match the specified value" do
    expect(store).to receive(:fetch).with(keys[:version3]).and_yield
    expect(store).to receive(:write).with(keys[:version3], '_specified_value_')

    expect(subject_experiment.cache_variant('_specified_value_') { '_new_value_' }).to eq('_specified_value_')
  end

  context "when there are migrations" do
    before do
      subject_experiment.context(migrated_with: { version: 2 })
      subject_experiment.context(migrated_with: { version: 1 })
    end

    it "returns the first found previous cached variant" do
      expect(store).to receive(:read).with(keys[:version2]).and_return(nil)
      expect(store).to receive(:read).with(keys[:version1]).and_return('_version_1_')

      expect(subject_experiment.cache_variant { '_new_value_' }).to eq('_version_1_')
    end

    it "caches the old variant to the new key" do
      expect(store).to receive(:read).with(keys[:version1]).and_return('_version_1_')
      expect(store).to receive(:write).with(keys[:version3], '_version_1_')
      expect(store).to receive(:delete).with(keys[:version1])

      expect(subject_experiment.cache_variant { '_new_value_' }).to eq('_version_1_')
    end

    it "falls through to using fetch if no previous cached variant is found" do
      expect(store).to receive(:read).with(keys[:version1])
      expect(store).to receive(:read).with(keys[:version2])
      expect(store).to receive(:fetch).with(keys[:version3]).and_yield

      expect(subject_experiment.cache_variant { '_new_value_' }).to eq('_new_value_')
    end
  end

  describe "the friendly interface" do
    let(:key) { "#{subject_experiment.name}:#{key_for(subject_experiment)}" }

    it "can read the cached value" do
      expect(store).to receive(:read).with(key).and_return('_value_')

      expect(subject_experiment.cache.read).to eq('_value_')
    end

    it "can write a value to the cache" do
      expect(store).to receive(:write).with(key, '_value_')

      subject_experiment.cache.write('_value_')
    end

    it "can write the experiment variant name if no value is provided" do
      subject_experiment.assigned(:my_variant_name)

      expect(store).to receive(:write).with(key, :my_variant_name)

      subject_experiment.cache.write
    end

    it "can delete the cached value" do
      expect(store).to receive(:delete).with(key)

      subject_experiment.cache.delete
    end

    it "can get set arbitrary hash attributes" do
      expect(store).to receive(:read).with("#{subject_experiment.name}_attrs:attr_name")

      subject_experiment.cache.attr_get(:attr_name)
    end

    it "can get an arbitrary hash attribute" do
      expect(store).to receive(:write).with("#{subject_experiment.name}_attrs:attr_name", '_value_')

      subject_experiment.cache.attr_set(:attr_name, '_value_')
    end

    it "can increment an arbitrary hash attribute" do
      expect(store).to receive(:increment).with("#{subject_experiment.name}_attrs:attr_name", 2)

      subject_experiment.cache.attr_inc(:attr_name, 2)
    end
  end
end
