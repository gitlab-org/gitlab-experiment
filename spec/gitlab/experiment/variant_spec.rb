# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Gitlab::Experiment::Variant do
  subject { described_class.new(name: '_name_', payload: { foo: 'bar' }) }

  it "has the expected members" do
    expect(described_class.members).to eq([:name, :payload])
  end

  it "uses kwargs" do
    expect(subject.name).to eq('_name_')
  end

  it "understands the group it belongs to" do
    expect(subject.group).to eq(:experiment)

    subject.name = 'control'
    expect(subject.group).to eq(:control)
  end
end
