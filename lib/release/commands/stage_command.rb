# frozen_string_literal: true

require 'thor'

module Release
  class CLI < Thor
    desc 'stage', 'Stage the release by creating the branches and appropriate merge requests'

    option :version, required: true, default: gemspec.version.to_s, desc: 'The version for release'
    option :push_to_gitlab, type: :boolean, required: false, default: false, desc: 'Push new version to GitLab'
    option :milestone, desc: 'The current or next milestone'
    option :update, type: :boolean, default: false, desc: 'Update existing branches'

    def stage
      return unless yes?("CLI release is deprecated. Do you want to continue?")

      say <<~MESSAGE
        Welcome! This command will ask a series of questions to walk you through the necessary steps to
        release #{gemspec.name} version #{version}.

      MESSAGE

      unless ENV['CI']
        stage_checkout_release_branch
        stage_update_the_version_file
        stage_ensure_changelog_entries
        stage_commit_and_push_any_changes
        stage_handle_project_merge_request
      end

      if (ENV['CI'] && !@project_updated) || (!ENV['CI'] && @project_updated)
        return unless yes?("Ready to open the gitlab MR to stage version #{version} of the #{gemspec.name} gem?")

        @project_mr = existing_merge_request(project_path, release_branch, state: 'opened')
        fail(<<~MESSAGE) unless @project_mr
          no merge request found on #{project_path} for #{release_branch} - run `bin/stage release` locally first
        MESSAGE

        if options[:push_to_gitlab]
          stage_push_any_updates_to_gitlab
          stage_handle_gitlab_merge_request
        end
      end

      say_status :success, "#{version} staged successfully"
      say <<~MESSAGE
        Congratulations, the release has been staged successfully!

        project: #{@project_mr&.web_url}
        gitlab: #{@gitlab_mr&.web_url}

        When the gitlab MR has passed CI, and has been approved, you can run the finalize command.
      MESSAGE
    end

    private

    def stage_checkout_release_branch
      return say_status :ok, "already on #{release_branch}" if current_branch == release_branch
      return unless yes?(<<~MESSAGE, fail: 'ok, not continuing')
        It looks like you're not on a release branch.
        Are you ready to checkout the "#{release_branch}" branch, creating it if needed?"
      MESSAGE

      git %(checkout "#{release_branch}"), require_success: false do |success, _result|
        git %(checkout -b "#{release_branch}") unless success
      end
    end

    def stage_update_the_version_file
      return say_status :ok, "#{version_file} already up to date" if version == gemspec.version.to_s

      gsub_file version_file, /VERSION = '[\d.]+'/, "VERSION = '#{version}'"
    end

    def stage_ensure_changelog_entries
      if exists?(changelog_file)
        say_status :ok, "#{changelog_file} already exists"
        return unless !options[:force] && yes?(<<~MESSAGE)
          It looks like a changelog file already exists for this version.
          Would you like to provide some new entries?
        MESSAGE

        insert_into_file changelog_file, "- #{changelog_entries.join("\n- ")}\n"
      else
        say_status :warn, "#{changelog_file} not found", :yellow
        return unless yes?(<<~MESSAGE, fail: 'ok, not continuing')
          No changelog file exists yet for this version.
          Would you like to provide some entries and create it now?
        MESSAGE

        create_file changelog_file, "### #{version} Changelog\n\n- #{changelog_entries.join("\n- ")}\n"
      end
    end

    def stage_commit_and_push_any_changes
      unstaged = any_unstaged_files?
      unpushed = any_unpushed_changes?

      return say_status :ok, 'no changes to commit or push' unless unstaged || unpushed
      return unless yes?(<<~MESSAGE, fail: 'ok, not continuing')
        It looks like there's some things that need to be commited and/or pushed.
        Ready to commit and push these changes?
      MESSAGE

      if unstaged
        git 'add .'
        git %(commit -am "Stage #{version} for release")
      end

      return unless unstaged || unpushed

      git 'push'
      @project_updated = true
    end

    def stage_push_any_updates_to_gitlab
      return say_status :skip, 'unable to commit to or update gitlab when pretending', :yellow if options[:pretend]

      branch_to_clone = options[:update] ? gitlab_release_branch : default_branch
      create_gemfile_commit(gemfile_line(url: project_url), from_branch: branch_to_clone)
    end

    def stage_handle_project_merge_request
      details = project_merge_request_options.merge(extra: changelog_content)
      @project_mr = existing_merge_request(project_path, release_branch, state: 'opened')

      if @project_mr
        say_status :ok, "project merge request exists - #{@project_mr.web_url}"
        return unless yes?(<<~MESSAGE)
          It looks like there's an existing merge request for this release.
          Would you like to update it?
        MESSAGE

        update_merge_request(@project_mr, details)
      else
        @project_mr = create_merge_request(project_path, release_branch, **details)
      end
    end

    def stage_handle_gitlab_merge_request
      details = gitlab_merge_request_options.merge(extra: changelog_content)
      @gitlab_mr = existing_merge_request(gitlab_path, gitlab_release_branch, state: 'opened')

      if @giltab_mr
        say_status :ok, "gitlab merge request exists - #{@gitlab_mr.web_url}"
        return unless yes?(<<~MESSAGE)
          It looks like there's an existing merge request for this release.
          Would you like to update it?
        MESSAGE

        update_merge_request(@gitlab_mr, details)
      else
        @gitlab_mr = create_merge_request(gitlab_path, gitlab_release_branch, **details)
      end
    end
  end
end
