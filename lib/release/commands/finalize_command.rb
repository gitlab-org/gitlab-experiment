# frozen_string_literal: true

require 'thor'

module Release
  class CLI < Thor
    desc 'finalize', 'Finalize and cut the gem release'

    option :version, required: true, default: gemspec.version.to_s, desc: 'The version for release'
    option :push_to_gitlab, type: :boolean, required: false, default: false, desc: 'Push new version to GitLab'

    def finalize
      return unless yes?("CLI release is deprecated. Do you want to continue?")

      if ENV['CI']
        say <<~MESSAGE
          This command builds and releases the gem, and updates any open MRs created using the stage
          command.

        MESSAGE

        return unless yes?("Ready to finalize version #{version} of the #{gemspec.name} gem?")

        begin
          finalize_checkout_release_branch
        rescue RuntimeError => e
          # this can fail on CI because there's no HEAD ref
          raise e unless e.message.include?('ref HEAD is not a symbolic ref')
        end

        finalize_build_and_push_gem
        finalize_create_package_artifact
        finalize_create_release_with_assets

        # maybe need a couple minute pause here, because rubygems uses a cache that can make
        # the gem server say the newly release version isn't found -- they've improved this
        # in the past year or two, but it can still be problematic for timing.

        finalize_push_any_updates_to_gitlab if options[:push_to_gitlab]
        finalize_accept_project_merge_request

        # remaining automation:
        #   - merge the project MR, because we're done with it.
        #   - update the gitlab MR to not be draft, and adjust the compare link to reference
        #     the new tag since the, release branch is no longer relevant and can be deleted.

        say <<~MESSAGE
          Congratulations! Version #{version} of #{gemspec.name} has been fully released.

        MESSAGE
      else
        say <<~MESSAGE
          This command creates a git tag that will trigger CI to build and release the final release
          of version #{version} of the #{gemspec.name} gem.

        MESSAGE

        return unless yes?("Ready to finalize version #{version} of the #{gemspec.name} gem?")

        finalize_checkout_release_branch
        finalize_create_git_release_tag

        say <<~MESSAGE
          The git tag for the release has been created! CI will do the remainder of the work finalizing
          the #{version} release.

          pipeline: #{project_url("-/pipelines?page=1&scope=all&ref=#{version(prefix: true)}")}
        MESSAGE
      end
    end

    private

    def finalize_checkout_release_branch
      return say_status :ok, "already on #{release_branch}" if current_branch == release_branch
      git %(checkout "#{release_branch}"), require_success: true
    end

    def finalize_create_git_release_tag
      git %(tag -a #{version(prefix: true)} -m "Version #{version}")
      git "push origin #{version(prefix: true)}"
    end

    def finalize_build_and_push_gem
      gem %(build --force --output="#{gem_filename}")
      gem %(push "#{gem_filename}")
    end

    def finalize_create_package_artifact
      say_status(:curl, "uploading #{gem_filename} artifact")
      run_command(
        :curl,
        %(--header "#{gitlab.curl_token}" --upload-file "#{gem_filename}" "#{artifact_url}"),
        verbose: false
      )
    end

    def finalize_create_release_with_assets
      with_gitlab("creating release for #{project_path} with assets") do |gitlab|
        gitlab.post "/projects/#{gitlab.url_encode(project_path)}/releases", body: {
          description: changelog_content,
          tag_name: version(prefix: true),
          assets: {
            links: [name: gem_filename, url: artifact_url]
          }
        }
      end
    end

    def finalize_push_any_updates_to_gitlab
      return say_status :skip, 'unable to commit to or update gitlab when pretending', :yellow if options[:pretend]

      create_gemfile_commit(gemfile_line, from_branch: gitlab_release_branch)
    end

    def finalize_accept_project_merge_request
      with_gitlab("accepting merge request for #{project_path} #{release_branch}") do |gitlab|
        project_mr = existing_merge_request(project_path, release_branch, state: 'opened')

        break say_status :ok, 'already merged or closed' unless project_mr

        gitlab.accept_merge_request(project_path, project_mr.iid)
      end
    end
  end
end
